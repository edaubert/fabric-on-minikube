#!/bin/bash

# I assume minikube is already installed and available in path

# TODO start minikube
# minikube start

# TODO start NFS server
# docker run -t -i --rm --name nfs --privileged -v `pwd`/3/share/:/opt/share -e SHARED_DIRECTORY=/opt/share -p 2049:2049 itsthenetwork/nfs-server-alpine:latest

# Test
# /opt/share is used by the script I got so before modifying it it is easier to use it as is.
# mkdir /opt/share
# sudo mount -t nfs4 <container ip>:/opt/share /opt/share
# sudo mount -t nfs4 <host ip>:/opt/share /opt/share
# sudo umount /opt/share

cd 3/

# get hyperledger Fabric binary
curl https://nexus.hyperledger.org/content/repositories/releases/org/hyperledger/fabric/hyperledger-fabric/linux-amd64-1.3.0/hyperledger-fabric-linux-amd64-1.3.0.tar.gz | tar xz

chmod +x bin/*


cd setupCluster
# TODO get minikube ip and use it on template
# minikube_ip=`minikube ip`
# currently you need to set it manually on
# templates/fabric_1_0_template_pod_cli.yaml
# templates/fabric_1_0_template_pod_namespace.yaml

# generate config which will be shared between pods
# This configuration is the Fabric's one.
bash generateALL.sh

# use the templates to build the Kubernetes definitions
python3 transform/run.py

while [[ `kubectl get pods --all-namespaces | grep -v NAMESPACE | grep -v "Running" | wc -l` -ne 0 ]]
do
  echo "Waiting for all pods to be running !"
  sleep 10
done

# TODO not tested yet

echo "DONE"
