# Hyperledger Fabric on Minikube

This project was a challenge which was part of a job interview process.

## Objectives

The main goal of this challenge was to explore how to deploy a Hyperledger Fabric network inside a Kubernetes cluster and more specifically on Minikube.

### [Hyperledger Fabric](https://www.hyperledger.org/projects/fabric)

> Hyperledger Fabric is a blockchain framework implementation and one of the Hyperledger projects hosted by The Linux Foundation. Intended as a foundation for developing applications or solutions with a modular architecture, Hyperledger Fabric allows components, such as consensus and membership services, to be plug-and-play. Hyperledger Fabric leverages container technology to host smart contracts called “chaincode” that comprise the application logic of the system. Hyperledger Fabric was initially contributed by Digital Asset and IBM, as a result of the first hackathon.

### [Kubernetes](https://kubernetes.io/)

> Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications.

### [Minikube](https://github.com/kubernetes/minikube).

> Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your laptop for users looking to try out Kubernetes or develop with it day-to-day.

## Retrieved information

I found multiple posts about how to run Hyperledger Fabric on Kubernetes cluster.

I even found a project which allow to deploy it on Minikube. However this project does not work directly. This project is named [Kubechain](https://github.com/kubechain/kubechain) and wants to provide an easy way to deploy blockchain framework over Kubernetes. As the aim is to tackle various blockchain framework, it introduce an overhead which I choose to avoid during this challenge. However the idea is quite interesting and should be deepened.

The first idea I crossed is about using Docker in Docker (DinD) approach. As Fabric run *chaincode* as docker container, we can host the peer node and give him DinD capabilities. However some changes needs to be applied on the Kubernetes cluster which, according to the author of this [blog post](https://medium.com/kokster/simpler-setup-for-hyperledger-fabric-on-kubernetes-using-docker-in-docker-8346f70fbe80), is not standard and implies to restart the overall cluster.


I also found three other blog posts which give some information about how to do it. However non of them talk about doing it on Minikube:
1. <a name="1"></a>[https://opensource.com/article/18/4/deploying-hyperledger-fabric-kubernetes](https://opensource.com/article/18/4/deploying-hyperledger-fabric-kubernetes) (April 2018)
2. <a name="2"></a>[https://www.techracers.com/blogs/hyperledger-fabric-cluster-kubernetes/](https://www.techracers.com/blogs/hyperledger-fabric-cluster-kubernetes/) (September 2018)
3. <a name="3"></a>[http://www.think-foundry.com/deploy-hyperledger-fabric-on-kubernetes-part-1/](http://www.think-foundry.com/deploy-hyperledger-fabric-on-kubernetes-part-1/) with
[http://www.think-foundry.com/deploy-hyperledger-fabric-on-kubernetes-part-2/](http://www.think-foundry.com/deploy-hyperledger-fabric-on-kubernetes-part-2/) (September 2017)

Finally, the website of Hyperledger Fabric also have a [blog post](https://www.hyperledger.org/blog/2018/11/15/how-to-deploy-hyperledger-fabric-on-kubernetes-part-ii) about it and was release on the end of October 2018.


## What I do

I first run the "First Network" use case with the global script and then with the manual way to know all the commands related to this use case.

Then I get the code of [2.](#2) and [3.](#3) and run them to see how it works on Minikube.

I do not test the one proposed on the Hyperledger website as it seems a bit complicated integrating Kafka stuff that I don't want for the first prototype. However it should be properly tested as well. But as it doesn't find a "simple" script to run, it will take more time to run it.

Both [2.](#2) and [3.](#3) use python to build the Kubernetes configuration. They also are using template to build the Kubernetes pods. But [2.](#2) defines for each possible configuration at least one template. [3.](#3) doesn't as it is simpler.

The other difference is that [3.](#3) is using a NFS server to share configurations between pods while [2.](#2) is only using Persistent volume claims (PVC).

At the end, both approach are not working out of the box. [2.](#2) had issue about channel creation but I did not go through this issue. [3.](#3) is not working due to issue about the NFS server reachability. Indeed even if I configure my host machine as NFS server, the fact is we have multiple networks involved here and I did not complete the configuration to get the NFS server available from the Kubernetes network. I think a good way to do could be to run the NFS server inside Kubernetes so it will be available inside the cluster without doing so much configuration about it.

## What next

If I had to choose between both approach, I will choose [3.](#3) if I only need to run this example because the code is a bit simpler and fit the "First Network" usecase. But if I want to manage multiple usecases, I will choose to explore [2.](#2).

Both [2.](#2) and [3.](#3) are about running specific Fabric network on Kubernetes. If we want a generic tool, we need to extend one of them to run easily every usecase. To do so some work about automation needs to be done.

While some code is made to automate the creation of pods, some code is still specific to the usecase. Especially the one about channel definitions.
We should think about a way to automatically generate them.

In [2.](#2), the author integrate *extraPods* but both approach are not managing the overall capabilities of Fabric. According to our needs, we may need to add some of the capabilities.

If we want to port this approach for every Kubernetes cluster, we need to think about security. While Minikube is for development and stay in local machine, other Kubernetes cluster will need to take more care about security.

Finally it could be interesting to build an helm package for this so that everyone culd be able to use it. Maybe this [post](http://www.think-foundry.com/hyperledger-fabric-deployment-using-helm-chart/) can help us to do it but I did not test it.
